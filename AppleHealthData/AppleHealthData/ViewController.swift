//
//  ViewController.swift
//  AppleHealthData
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/27/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import HealthKit

typealias HKCompletionHandleForStep = (Double?, NSError?) -> ()

class ViewController: UIViewController {
    
    @IBOutlet weak var stepsTodayLabel : UILabel?
    @IBOutlet weak var heartrateLabel : UILabel?
    @IBOutlet weak var caloriesBmrLabel : UILabel?
    @IBOutlet weak var caloriesLabel : UILabel?
    @IBOutlet weak var sleepLabel : UILabel?
    @IBOutlet weak var profileLabel : UILabel?
    @IBOutlet weak var restingCalorie : UILabel?
    
    var previousCurrentValue : Float = 0.0
    var healthStore: HKHealthStore? = HKHealthStore()
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let completion: ((Bool, Error?) -> Void)! = {
            (success, error) -> Void in
            if !success {
                print("You didn't allow HealthKit to access these read/write data types. In your app, try to handle this error gracefully when a user decides not to provide access. The error was: \(error). If you're using a simulator, try it on a device.")
                
                return
            } else {
            }
            
            DispatchQueue.main.async{
                // Update the user interface based on the current user's health information.
                self.calculateBMR()
            }
        }
        self.getHealthKitPermission(completion: completion)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHealthKitPermission(completion:((Bool, Error?) -> Void)!) {
        guard HKHealthStore.isHealthDataAvailable() else {
            
            return
        }
        
        let readDataTypes: Set<HKObjectType> = self.dataTypesToRead()
        self.healthStore?.requestAuthorization(toShare: nil, read: readDataTypes, completion: completion)
    }
    
    private func dataTypesToRead() -> Set<HKObjectType> {
        
        let dietaryCalorieEnergyType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryEnergyConsumed)!
        let activeEnergyBurnType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let heightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let weightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let birthdayType = HKQuantityType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!
        let biologicalSexType = HKQuantityType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let stepCountType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let basalEnregyType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!
        
        let readDataTypes: Set<HKObjectType>
        
        if #available(iOS 9.3, *) {
            let activeTimeType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)!
            readDataTypes = [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType,heartRateType,stepCountType,activeTimeType,basalEnregyType]
            
        } else {
            // Fallback on earlier versions
            readDataTypes = [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType,heartRateType,stepCountType,basalEnregyType]
        }
        
        return readDataTypes
    }
    
    func calculateBMR(){
        var sexString = ""
        //Get Date of birth
        var dateOfBirth: Date!
        do {
            dateOfBirth = try self.healthStore!.dateOfBirth()
        } catch {
            return
        }
        
        //Calculate Age
        
        let nowDate = Date()
        let ageComponents: DateComponents = Calendar.current.dateComponents([Calendar.Component.year], from: dateOfBirth, to: nowDate)
        let ageInYears: Int = ageComponents.year!
        
        print("Age \(ageInYears)")
        
        //Get Biological Sex
        var biologicalSexObjet: HKBiologicalSexObject!
        
        do {
            biologicalSexObjet = try self.healthStore!.biologicalSex()
            if biologicalSexObjet.biologicalSex == .male {
                sexString = "M"
            } else {
                sexString = "F"
            }
        } catch {
            return
        }
        
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        let weightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let heightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        
        var heightInCentimeters: Double = 0.0
        var weightInKilGrams : Double = 0.0
        
        let queryWeight : HKCompletionHandle = {
            (weight , error) -> Void in
            guard let weight = weight else {
                return
            }
            weightInKilGrams = weight.doubleValue(for: HKUnit.gramUnit(with: HKMetricPrefix.kilo))
            print("Weight \(weightInKilGrams)")
            
            let queryheartRate : HKCompletionHandle = {
                (heartRate , error) -> Void in
                guard let heartRate = heartRate?.doubleValue(for: HKUnit(from:"count/min")) else {
                    return
                }
                self.heartrateLabel?.text = String(format: "%.0f",heartRate)
                print("Heart Rate \(heartRate)")
                
                
                let queryHeight : HKCompletionHandle = {
                    (height , error) -> Void in
                    guard let height = height else {
                        return
                    }
                    heightInCentimeters = height.doubleValue(for: HKUnit(from:"cm"))
                    print("Height \(heightInCentimeters)")
                    
                    let totalStepCount : HKCompletionHandleForStep = {
                        (steps , error) -> Void in
                        guard let steps = steps else {
                            return
                        }
                        self.stepsTodayLabel?.text = String(format: "%.0f",steps)
                        print("steps \(steps)")
                        let activeEnergyBurned : HKCompletionHandleForStep = {
                            (calorie , error) -> Void in
                            guard let calorie = calorie else {
                                return
                            }
                            self.caloriesLabel?.text = String(format: "%.0f",calorie)
                            self.profileLabel?.text = String(format: "%.0fkg,%.0fcm,%dYears,%@", weightInKilGrams,heightInCentimeters,ageInYears,sexString)
                            var bmr : Double = 0.0
                            
                            if (biologicalSexObjet.biologicalSex == .male) {
                                let w = (13.397 * weightInKilGrams)
                                let h = (4.799 * heightInCentimeters)
                                let a = (5.677 * Double(ageInYears))
                                bmr = 88.362 +  +w +  h - a
                            }else {
                                let w = (9.247 * weightInKilGrams)
                                let h = (3.098 * heightInCentimeters)
                                let a = (5.677 * Double(ageInYears))
                                bmr = 447.593 +  +w +  h - a
                            }
                            
                            self.caloriesBmrLabel?.text = String(format:"%.0f",bmr)
                            print("Total Active Energy \(calorie)")
                            
                            let queryBasalEnergy : HKCompletionHandleForStep = {
                                (basalEnergy , error) -> Void in
                                guard let basalEnergy = basalEnergy else {
                                    return
                                }
                                self.restingCalorie?.text = String(format: "%0.f", basalEnergy)
                                print("Basal Energy :\(basalEnergy)")
                            }
                            //Calculate Basal Energy
                            self.basalEnergyBurned(completion: queryBasalEnergy)
                        }
                        self.activeEnergyBurned(completion: activeEnergyBurned)
                    }
                    self.recentSteps2(completion: totalStepCount)
                }
                //Height
                if let healthStore = self.healthStore {
                    healthStore.mostRecentQuantitySample(ofType: heightType, predicate: nil, completion: queryHeight)
                }
            }
            
            if let healthStore = self.healthStore {
                healthStore.mostRecentQuantitySample(ofType: heartRateType, predicate: nil, completion: queryheartRate)
            }
        }
        
        let queryActiveTime : HKCompletionHandle = {
            (activeTime , error) -> Void in
            guard let activeTime = activeTime else {
                return
            }
            print("Active Time \(activeTime)")
        }
        
        //Weight
        if let healthStore = self.healthStore {
            healthStore.mostRecentQuantitySample(ofType: weightType, predicate: nil, completion: queryWeight)
        }
        
        //Active Excercise Time
        if #available(iOS 9.3, *) {
            let activeTime = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)!
            if let healthStore = self.healthStore {
                healthStore.mostRecentQuantitySample(ofType: activeTime, predicate: todayPredicate, completion: queryActiveTime)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func predicateForSamplesToday() -> NSPredicate
    {
        let (starDate, endDate): (Date, Date) = self.datesFromToday()
        
        let predicate: NSPredicate = HKQuery.predicateForSamples(withStart: starDate, end: endDate, options: HKQueryOptions.strictStartDate)
        
        return predicate
    }
    
    private func datesFromToday() -> (Date, Date)
    {
        let calendar = Calendar.current
        
        let nowDate = Date()
        
        let starDate: Date = calendar.startOfDay(for: nowDate)
        let endDate: Date = calendar.date(byAdding: Calendar.Component.day, value: 1, to: starDate)!
        return (starDate, endDate)
    }
    
    func recentSteps2(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var steps: Double = 0
            
            if (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    //Duplicate in the sample can occur , so it can be restricted to choose any source
                    //if result.sourceRevision.source.name == "Vinodha's Apple Watch" {
                    steps += result.quantity.doubleValue(for: HKUnit.count())
                    //}
                }
            }
            completion(steps, error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    func activeEnergyBurned(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var calories : Double = 0
            
            if (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    calories += result.quantity.doubleValue(for:HKUnit.kilocalorie())
                }
            }
            completion(Double(calories), error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    func basalEnergyBurned(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var calories : Double = 0
            
            if (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    calories += result.quantity.doubleValue(for:HKUnit.kilocalorie())
                }
            }
            completion(Double(calories), error as NSError?)
        }
        self.healthStore?.execute(query)
    }
}


